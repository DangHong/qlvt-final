#include "vattu.h"

using namespace std;

void NhapSoluongXuat (int &num, int slt){
	cout <<endl<< "Nhap vao so luong: "; 
    cin >> num;
    while(num > slt){
    	BaoLoi ("So luong xuat ra lon hon so luong ton kho!");
    	cout << "So luong ton kho: " << slt << endl;
    	cout << "Nhap lai so luong : ";
    	cin >> num;
	}
}

void NhapSoLuong (int &num)
{
	cout <<endl<< "Nhap vao so luong: "; 
    cin >> num;
}

void NhapDonGia (long &dongia)
{
	cout << "Nhap don gia: "; 
	cin >> dongia;
}

void NhapVAT (float &vat)
{
	cout << "Nhap thue VAT: "; 
	cin >> vat;
}

void NhapTrangThai (char *status){
	cout << "Nhap vao trang thai (1 hoac 0): "; fflush(stdin);
	gets(status); 
	while ((strcmp(status,"1")!=0) && (strcmp(status,"0")!=0)){
		cout << "Trang thai khong dung. Nhap vao trang thai (1 hoac 0): ";  fflush(stdin);
		gets(status);
	}
}

void TaoTieuDeVT(int dem)
{
	int j;
	if (dem == 0)
	{
		gotoxy(3,3);
		cout<<"MA VT : ";
		gotoxy(3,5);
		cout<<"Ten VT :";
		gotoxy(3,7);
		cout<<"Dvt :";
		gotoxy(3,9);
		cout<<"SLT :";
	}
	else 
	{
		gotoxy(3,dem*15);
		cout<<"MA VT : ";
		gotoxy(3,dem*15 +2);
		cout<<"Ten VT :";
		gotoxy(3,dem*15 +4);
		cout<<"Dvt :";
		gotoxy(3,dem*15+6);
		cout<<"SLT :";
	}	
}
void HienVatTu(VATTU *vt, int dem)
{
	if (dem == 0)
	{
		gotoxy(12,3);
		cout<<vt->MAVT;
		gotoxy(12,5);
		cout<< vt->TENVT;
		gotoxy(10,7);
		cout<<vt->DVT;
		gotoxy(10,9);
		cout<<vt->SLT;
		}
	else 
	{
		gotoxy(12,dem*15 );
		cout<<vt->MAVT;
		gotoxy(12,dem*15+2 );
		cout<< vt->TENVT;
		gotoxy(10,dem*15+4 );
		cout<<vt->DVT;
		gotoxy(10,dem*15+6);
		cout<<vt->SLT;
	}
}

void UpdateSoLuongVtNhap(CT_HOADON cthd,DSVT &dsvt)
{	
	int index = SearchVatTu(dsvt,cthd.MAVT);
	dsvt.nodes[index]->SLT = dsvt.nodes[index]->SLT + cthd.Soluong;
}

void UpdateSoLuongVtXuat(CT_HOADON cthd,DSVT &dsvt)
{
	int index = SearchVatTu(dsvt,cthd.MAVT);
	dsvt.nodes[index]->SLT = dsvt.nodes[index]->SLT - cthd.Soluong;
}

void NhapCTHDXuat(DSVT &dsvt,LCTHD &listCTHD){
	
	string maVt;
	int dem = 0;
	listCTHD.n = 0;
	do{
		if(listCTHD.n == 20){
			BaoLoi("Vuot qua gioi han 20 vat tu!");
			cout<<" An phim bat ky de tiep tuc !";
			getch();
			break;
		}
		cout << "Nhap vao ma vat tu (0 la ket thuc): ";
		fflush(stdin);
		getline(cin,maVt);
		if (DemKyTu(maVt)<10){
		
			CT_HOADON cthd;
			strcpy(cthd.MAVT,(char*)maVt.c_str());
			if(strcmp(cthd.MAVT,"0") != 0)
			{	
				int search = SearchVatTu(dsvt,(char*)maVt.c_str());
				if(search<0){
					BaoLoi("Ma vat tu khong ton tai");
				}else
				{
					
					TaoTieuDeVT(dem);
					HienVatTu(dsvt.nodes[search],dem);
					dem++;
					NhapSoluongXuat(cthd.Soluong,dsvt.nodes[search]->SLT);
					NhapDonGia(cthd.Dongia);
					NhapVAT(cthd.VAT);
					NhapTrangThai(cthd.status);
					
					listCTHD.nodes[listCTHD.n] = cthd;
					listCTHD.n++;
					// Cap nhat so luong ton kho cho Vat tu
					UpdateSoLuongVtXuat(cthd,dsvt);
				}
			}
		}else{
			BaoLoi("So ky tu vuot qua quy dinh (10 ky tu)!");
		}
	
	}while(strcmp((char*)maVt.c_str(),"0") != 0);
}

void NhapCTHDNhap(DSVT &dsvt,LCTHD &listCTHD){
	
	string maVt;
	int dem = 0;
	listCTHD.n = 0;
	do{
		if(listCTHD.n == 20){
			BaoLoi("Vuot qua gioi han 20 vat tu!");
			cout<<" An phim bat ky de tiep tuc !";
			getch();
			break;
		}
		cout << "Nhap vao ma vat tu (0 la ket thuc): ";
		fflush(stdin);
		getline(cin,maVt);
		if (DemKyTu(maVt)<10){
		
			CT_HOADON cthd;
			strcpy(cthd.MAVT,(char*)maVt.c_str());
			if(strcmp(cthd.MAVT,"0") != 0)
			{	
				int search = SearchVatTu(dsvt,(char*)maVt.c_str());
				if(search<0){
					BaoLoi("Ma vat tu khong ton tai");
				}else
				{
					
					TaoTieuDeVT(dem);
					HienVatTu(dsvt.nodes[search],dem);
					dem++;
					NhapSoLuong(cthd.Soluong);
					NhapDonGia(cthd.Dongia);
					NhapVAT(cthd.VAT);
					NhapTrangThai(cthd.status);
					
					listCTHD.nodes[listCTHD.n] = cthd;
					listCTHD.n++;
					// Cap nhat so luong ton kho cho Vat tu
					UpdateSoLuongVtNhap(cthd,dsvt);
				}
			}
		}else{
			BaoLoi("So ky tu vuot qua quy dinh (10 ky tu)!");
		}
	
	}while(strcmp((char*)maVt.c_str(),"0") != 0);
}

