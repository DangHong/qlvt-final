#include "nhanvien.h"
using namespace std;

char *duongDanFile = "DSHD.txt";

Date LayGioHeThong()
{
	time_t t = time(0);
	struct tm *now = localtime(&t);
	Date date;
	date.ngay = now->tm_mday;
	date.thang = now->tm_mon + 1;
	date.nam = now->tm_year + 1900;
	return date;
}

void inTieude_HD(int j)
{
	gotoxy(2, j - 1);
	cout << "SO HOA DON";
	gotoxy(18, j - 1);
	cout << "NGAY NHAP";
	gotoxy(32, j - 1);
	cout << "LOAI";
}

void themVaoListHd(ListHD &listHd, HOADON hoaDon)
{
	NODEHD *nodeHd = new NODEHD();
	nodeHd->infohd = hoaDon;
	nodeHd->next = NULL;

	if (listHd.first == NULL)
	{
		listHd.first = nodeHd;
		listHd.last = nodeHd;
	}
	else
	{
		//tao lien ket giua last va phan tu them vao
		listHd.last->next = nodeHd;
		//cap nhat lai listhd
		listHd.last = nodeHd;
	}
}

int laySoPhanTuHd(NodeNV nvGhi){
	int dem = 0;
	NODEHD *p = new NODEHD();
	p = nvGhi->nhanvien.listHD.first;
	while(p != NULL){
		dem++;
		p = p->next;
	}
	return dem;
}

void ghiFileDshd(NodeNV listNv,fstream &fileDsHoaDon)
{
	string recordHd = "";
	string chamPhay = ";";
	string tab = "\t";
	string dauThang = "#";
	string recordCthd = "";
	
	if(listNv != NULL){
		// Duyet qua ca list, ghi vao file
		NODEHD *pDuyet = listNv->nhanvien.listHD.first;
		int soPhanTu = 0;
		soPhanTu = laySoPhanTuHd(listNv);
		fileDsHoaDon << convertIntToString(listNv->nhanvien.MANV) << chamPhay << soPhanTu << endl;
		while (pDuyet != NULL)
		{
			recordHd = dauThang + pDuyet->infohd.SoHD + chamPhay + ChuyenDateSangChuoi(pDuyet->infohd.ngaylap) + chamPhay + pDuyet->infohd.loai + chamPhay + convertIntToString(pDuyet->infohd.cthd.n);
			fileDsHoaDon << recordHd << endl;
			for (int i = 0; i < pDuyet->infohd.cthd.n; i++)
			{
				CT_HOADON cthd = pDuyet->infohd.cthd.nodes[i];
				recordCthd = tab + cthd.MAVT + chamPhay + convertIntToString(cthd.Soluong) + chamPhay + convertLongToString(cthd.Dongia) + chamPhay + convertFloatToString(cthd.VAT) + chamPhay + cthd.status;
				fileDsHoaDon << recordCthd << endl;
			}
			pDuyet = pDuyet->next;
		}
	ghiFileDshd(listNv->pLeft, fileDsHoaDon);
	ghiFileDshd(listNv->pRight, fileDsHoaDon);
	}
}

void LuuFileDSHD(NodeNV listNv){
	SaveFileNV(pNVList, "DSNV.TXT");
	
	fstream fileDsHoaDon;
	fileDsHoaDon.open(duongDanFile, ios::out | ios::ate);
	
	ghiFileDshd(listNv, fileDsHoaDon);
	fileDsHoaDon.close();
}

//ho tro cho doc file 
HOADON taoRecordHd(fstream &file)
{
	HOADON returnHd = {};
	LCTHD listCthd;
	listCthd.n = 0;
	string temp;
	int soRecordCthd;

	getline(file, temp, ';');
	temp = temp.substr(1);
	strcpy(returnHd.SoHD, (char *)temp.c_str());
	getline(file, temp, ';');
	returnHd.ngaylap = ChuyenChuoiSangDate((char *)temp.c_str());
	getline(file, temp, ';');
	strcpy(returnHd.loai, (char *)temp.c_str());
	getline(file, temp);
	soRecordCthd = atoi((char*)temp.c_str());
	
	// Lay list chi tiet hoa don cua returnHd 

	for (int i = 0; i < soRecordCthd; i++)
	{
		CT_HOADON ct_hd;

		getline(file, temp, ';');
		temp = temp.substr(1, temp.size() - 1);
		strcpy(ct_hd.MAVT, (char *)temp.c_str());
		getline(file, temp, ';');
		ct_hd.Soluong = atoi((char *)temp.c_str());
		getline(file, temp, ';');
		ct_hd.Dongia = atoi((char *)temp.c_str());
		getline(file, temp, ';');
		ct_hd.VAT = convertStringToFloat(temp);
		getline(file, temp);
		strcpy(ct_hd.status, (char *)temp.c_str());

		listCthd.nodes[listCthd.n] = ct_hd;
		listCthd.n++;
	}

	returnHd.cthd = listCthd;

	return returnHd;
}

void DocFileDsHoaDon(NodeNV &listNv)
{
	OpenFileNV(pNVList, "DSNV.TXT");
	
	fstream fileDsHoaDon;
	fileDsHoaDon.open(duongDanFile, ios::in);
	//lap qua file cho den khi gap ki tu ket thuc
	while (!fileDsHoaDon.eof())
	{
		string str;
		//lay 1 dong trong file
		getline(fileDsHoaDon, str);
		//tim dau ; / truoc ; la manv / sau ; la so phtu hoa don
		int index = str.find(";");
		
		int maNv = convertStringToInt(str.substr(0,index));
		int soLuongHd = convertStringToInt(str.substr(index + 1));
		
		NodeNV nvGhi = TimNV(listNv,maNv);
		if( nvGhi != NULL){
			for(int i = 0; i < soLuongHd; i++){
				HOADON hd = taoRecordHd(fileDsHoaDon);
				themVaoListHd(nvGhi->nhanvien.listHD, hd);
			}
		}
	}
	fileDsHoaDon.close();
}

int Search_HD(ListHD listHd, char *x)
{
	NODEHD *p = listHd.first;
	if (listHd.first == NULL)
	{
		return -1;
	}
	while (p != NULL)
	{
		if (strcmp(p->infohd.SoHD, x) == 0)
			return 1;
		p = p->next;
	}
	return -1;
}

void TieuDeLapHoaDon()
{
	gotoxy(3, 5);
	cout << "So HD : ";
	gotoxy(3, 7);
	cout << "Ngay Lap :";
	gotoxy(3, 9);
	cout << "MaNV Lap :";
	gotoxy(40, 5);
	cout << "Loai :";
}

void XuatThoiGian(Date h, int x, int y, bool noGoto)
{
	if(!noGoto){
		gotoxy(x, y);
	}
	if (h.ngay < 10 && h.thang >= 10)
	{
		cout << "0" << h.ngay << "/" << h.thang << "/" << h.nam;
	}
	else if (h.thang < 10 && h.ngay >= 10)
	{
		cout << h.ngay << "/0" << h.thang << "/" << h.nam;
	}
	else if (h.ngay < 10 && h.thang < 10)
	{
		cout << "0" << h.ngay << "/0" << h.thang << "/" << h.nam;
	}
	else
	{
		cout << h.ngay << "/" << h.thang << "/" << h.nam;
	}
}

void XuatTieuDe(HoaDon hd)
{
	gotoxy(12, 5);
	cout << hd.SoHD;
	XuatThoiGian(hd.ngaylap, 15, 7, false);
	gotoxy(47, 5);
	cout << hd.loai;
}

void TaoMaSoHoaDon(HOADON &hd)
{
	strcpy(hd.SoHD, convertIntToChar(mangSoHdNgauNhien[indexSoHd]));
	indexSoHd++;
}

void TTHD_Nhap(HOADON &hd)
{
	TaoMaSoHoaDon(hd);
	strcpy(hd.loai, "N");
	hd.ngaylap = LayGioHeThong();
	XuatTieuDe(hd);
}

void TTHD_Xuat(HOADON &hd)
{
	TaoMaSoHoaDon(hd);
	strcpy(hd.loai, "X");
	hd.ngaylap = LayGioHeThong();
	XuatTieuDe(hd);
}

void TaoTieuDeNhanVien()
{
	gotoxy(81, 5);
	cout << "Ma NV : ";
	gotoxy(81, 7);
	cout << "Ho NV : ";
	gotoxy(81, 9);
	cout << "Ten NV : ";
	gotoxy(81, 11);
	cout << "GT : ";
}

void XuatThongTinNhanVien(NHANVIEN nv)
{
	gotoxy(89, 5);
	cout << nv.MANV;
	gotoxy(89, 7);
	cout << nv.HO;
	gotoxy(91, 9);
	cout << nv.TEN;
	gotoxy(88, 11);
	cout << nv.PHAI;
}

void XoaManHinhTheoVung(int x, int y, int dong, int cot)
{
	for (int i = 1; i <= dong; i++)
	{
		gotoxy(x, y + i);
		for (int j = 1; j <= cot; j++)
		{
			cout << " ";
		}
	}
}

bool NhapMaSoNhanVien(NHANVIEN &nv, NodeNV rootNv)
{
	int k;
	NodeNV nodeCheck;
	bool timThay = true;
	do
	{
		
		fflush(stdin);
		gotoxy(17, 9);
		cin >> nv.MANV;
		if(nv.MANV != 0){
		    nodeCheck = TimNV(rootNv,nv.MANV);
			if(nodeCheck == NULL)
			{
				timThay = false;
				gotoxy(17, 9);
				cout << "                        ";
				TaoThongBaoVaMat(2, 28, "THONG BAO : Ma so nhan vien khong co !. Vui long nhap lai");
			}
			else
			{
				TaoThongBaoVaMat(2, 28, "THONG BAO : Ma so nhan vien da dung !");
				TaoFormNhap("Thong tin nhan vien", 80, 2, 30);
	
				TaoTieuDeNhanVien();
				XuatThongTinNhanVien(nodeCheck->nhanvien);
				nv = nodeCheck->nhanvien;
	
				gotoxy(2, 28);
				cout << "THONG BAO : An phim bat ky de tiep tuc !";
				sleep(2);
				getch();
				gotoxy(2, 28);
				cout << "                                          ";
				XoaManHinhTheoVung(90, 5, 15, 50);
				return true;
			}
			if (nv.MANV > 999)
			{
				gotoxy(17, 9);
				cout << "                         ";
				TaoThongBaoVaMat(2, 29, "THONG BAO : Qua so ky tu !. Vui long nhap lai");
			}
			else if (nv.MANV == 0)
			{
				gotoxy(17, 9);
				cout << "                                    ";
				TaoThongBaoVaMat(2, 29, "THONG BAO : !!!. Vui long nhap lai");
			}
		}
		timThay = true;
	} while (timThay == false || nv.MANV > 999 || nv.MANV != 0);
	gotoxy(2, 20);
	return false;
}
void InitHoaDon(ListHD &lhd)
{
	lhd.first = lhd.last = NULL;
}

void ThemVaoCuoi(ListHD &lhd, NODEHD *p)
{
	if (lhd.first == NULL)
	{
		lhd.first = lhd.last = p;
	}
	else
	{
		lhd.last->next = p;
		lhd.last = p;
	}
}

void LapHD_Nhap(NodeNV rootNv, DSVT &dsvt)
{
	system("cls");
	TaoFormNhap(" LAP HOA DON NHAP ", 2, 2, 50);
	ThongBao(2, 18, "LUU Y : Ma So nhan vien co 3 chu so :");
	TieuDeLapHoaDon();
	HOADON hoaDon;
	TTHD_Nhap(hoaDon);
	NHANVIEN nvThemHd;

	if(NhapMaSoNhanVien(nvThemHd, pNVList) == true){
		if (nvThemHd.listHD.first == NULL)
		{
			InitHoaDon(nvThemHd.listHD);
		}
		system("Cls");
		cout << "---NHAP CHI TIET HOA DON---" << endl;
		NhapCTHDNhap(dsvt,hoaDon.cthd);
		
		//Sau khi nhap cthd xong thi luu hoa don vao tao vao list nhanvien.listHD
		NODEHD *nodeHd = new NODEHD();
		nodeHd->infohd = hoaDon;
		nodeHd->next = NULL;
		
		NodeNV nodeNvThem = TimNV(pNVList,nvThemHd.MANV);
		ThemVaoCuoi(nodeNvThem->nhanvien.listHD,nodeHd);
	}	
}

void LapHD_Xuat(NodeNV rootNv, DSVT &dsvt)
{
	system("cls");
	TaoFormNhap(" LAP HOA DON XUAT ", 2, 2, 50);
	ThongBao(2, 18, "LUU Y : Ma So nhan vien co 3 chu so :");
	TieuDeLapHoaDon();
	HOADON hoaDon;
	TTHD_Xuat(hoaDon);
	NHANVIEN nvThemHd;

	if(NhapMaSoNhanVien(nvThemHd, rootNv) == true){
		if (nvThemHd.listHD.first == NULL)
		{
			InitHoaDon(nvThemHd.listHD);
		}
		system("Cls");
		cout << "---NHAP CHI TIET HOA DON---" << endl;
		
		NhapCTHDXuat(dsvt,hoaDon.cthd);
		
		NODEHD *nodeHd = new NODEHD();
		nodeHd->infohd = hoaDon;
		nodeHd->next = NULL;
		
		NodeNV nodeNvThem = TimNV(rootNv,nvThemHd.MANV);
		ThemVaoCuoi(nodeNvThem->nhanvien.listHD,nodeHd);
	}
}

void xuatListHoaDonTheoNhanVien(NodeNV root)
{
	cout << "Nhap ma Nv muon xem List HD :";
	int maNv;
	cin >> maNv;
	NodeNV nodeNvThem = TimNV(root,maNv);
	NODEHD *node = new NODEHD();
	node = nodeNvThem->nhanvien.listHD.first;

	while (node != NULL)
	{
		cout << node->infohd.SoHD << endl;
		node = node->next;
	}
}

bool TimHdTrongNv(NodeNV nv, char* soHd , HOADON &hoaDon){
	NODEHD *nodeHd = new NODEHD();
	nodeHd = nv->nhanvien.listHD.first;
	
	while(nodeHd!= NULL){
		if(strcmp(nodeHd->infohd.SoHD,soHd) == 0){
			hoaDon = nodeHd->infohd;
			return true;
		}
		nodeHd = nodeHd->next;
	}
	return false;
}
// Duyet trong cay Nhan vien de tim hoa don
bool DuyetCayNvTimHd(NodeNV root, char* soHd, HOADON &hoaDonCanTim){
	if(root!=NULL){
		
		if(TimHdTrongNv(root, soHd, hoaDonCanTim) == true){
			return true;
		}
		DuyetCayNvTimHd(root->pLeft, soHd, hoaDonCanTim);
		DuyetCayNvTimHd(root->pRight, soHd, hoaDonCanTim);
	}else{
		return false;
	}
}

void XemHdTheoSoHd(NodeNV root){ 
	string SoHd;
	bool check;
	do{
		cout<< "Nhap so hoa don muon xem (0 la thoat ra) : ";
		fflush(stdin);
		getline(cin,SoHd);

		if(strcmp((char*)SoHd.c_str(),"0") != 0){
			HOADON hd;
			check = DuyetCayNvTimHd(root,(char*)SoHd.c_str(),hd);
			if(check == true){ // neu tim duoc hoa don
				cout << "So HD: " << hd.SoHD;
				cout << "       Ngay lap: " ;
				XuatThoiGian(hd.ngaylap,0,0,true);
				cout << "       Loai: " << hd.loai << endl;
				for(int i =0 ; i <hd.cthd.n; i++ ){
					cout<< i+1 << "			   Ma vat tu"<<hd.cthd.nodes[i].MAVT<<endl;
					cout << "			   So luong	"<<hd.cthd.nodes[i].Soluong<<endl;
					cout << "			   Don gia 	"<<hd.cthd.nodes[i].Dongia<<endl;
					cout << "			   VAT		"<<hd.cthd.nodes[i].VAT<<endl;
					cout << "			   Status	"<<hd.cthd.nodes[i].status<<endl;
				}
				
			}else{
				cout << "Khong tim thay hoa don..."<<endl;
			}
		}
	}while(strcmp((char*)SoHd.c_str(),"0") != 0);
}


void inTieudeHoaDon(int j){
	gotoxy(2,j-1); cout<< "So HD";
	gotoxy(20, j-1); cout<<"Ngay lap";
	gotoxy(40, j-1); cout<<"Loai HD";
	gotoxy(60, j-1); cout<<"Tri gia hoa don";
}

void inHoaDonLietKe(int y, HOADON hd){
	gotoxy(2,y); cout<< hd.SoHD;
	gotoxy(20, y); XuatThoiGian(hd.ngaylap,0,0,true);
	gotoxy(40, y); cout<<hd.loai;
	
	int giaTri = 0;
	for (int i = 0; i < hd.cthd.n; i++){
		giaTri = giaTri + hd.cthd.nodes[i].Dongia*hd.cthd.nodes[i].Soluong + (hd.cthd.nodes[i].Dongia*hd.cthd.nodes[i].Soluong*hd.cthd.nodes[i].VAT)/100;
	}
	gotoxy(60, y); 
	cout<<giaTri << endl;
}

// hoi lai anh nguoi yeu
bool KiemTraThoiGian(Date dateFrom, Date dateTo, Date dateHd){
	
	int thangFrom = dateFrom.nam*12 + dateFrom.thang;
	int thangTo = dateTo.nam*12 + dateTo.thang;
	int thangHd = dateHd.nam*12 + dateHd.thang;
	
	if (thangFrom < thangHd && thangHd <= thangTo ){
		if(thangHd == thangTo){
			if(dateHd.ngay <= dateTo.ngay)
				return true;
		}else{
			return true;
		}
	}else if (thangFrom == thangHd && thangHd == thangTo){
		if (dateFrom.ngay <= dateHd.ngay && dateHd.ngay <= dateTo.ngay )
			return true;
	}else if (thangFrom <= thangHd && thangHd < thangTo){
		if(thangHd == thangFrom){
			if(dateHd.ngay >= dateFrom.ngay)
				return true;
		}else{
			return true;
		}
	}
	
	return false;
}

void InHoaDonTheoNgay(NHANVIEN nv, Date dateFrom, Date dateTo){
	bool coHd = false;
	system("cls");
	cout <<"                        BANG LIET KE CAC HOA DON TRONG KHOANG THOI GIAN                        " << endl;
	cout <<"                          Tu ngay : " ;
	XuatThoiGian(dateFrom, 0, 0, true);
	cout <<"  Den ngay : " ;
	XuatThoiGian(dateTo, 0, 0, true);
	cout << endl <<"                          Nhan vien : "<< nv.HO << " " << nv.TEN << endl;
	inTieudeHoaDon(4);
	int index = 0;
	
	if(nv.listHD.first == NULL){
		cout << "Khong co hoa don nao duoc tao boi nhan vien nay!";
	}
	else
	{
		NODEHD *nodeHd = new NODEHD();
		nodeHd = nv.listHD.first;
		while (nodeHd != NULL)
		{
			if (KiemTraThoiGian(dateFrom,dateTo,nodeHd->infohd.ngaylap) == true){
				coHd = true;
				inHoaDonLietKe(index + 4, nodeHd->infohd);
				index++;
			}
			
			nodeHd = nodeHd->next;
		}
		if(coHd == false){
			cout << "Khong co hoa don nao trong khoang thoi gian nay!";
		}
	}
	
	
}
void LietKeHoaDon(NodeNV listNv)
{
	int maNv;
	NodeNV nodeNv;
	do{
		cout << "Nhap vao ma nhan vien (0 la ket thuc): ";
		cin >> maNv;
		if(maNv != 0)
		{
			nodeNv = TimNV(listNv,maNv);
			if(nodeNv == NULL)
			{
				cout << "Ma so nhan vien khong co !"<<endl;
			}else
			{
				Date dateFrom;
				Date dateTo;
				if(NhapNgayThang("Nhap vao ngay bat dau DD/mm/yyyy (0 de ket thuc): ",dateFrom) == false){
					return;
				}
				if(	NhapNgayThang("Nhap vao ngay ket thuc DD/mm/yyyy (0 de ket thuc): ",dateTo) == false){
					return;
				}
				InHoaDonTheoNgay(nodeNv->nhanvien,dateFrom,dateTo);
				return;
			}
		}
	}while(maNv != 0);
}


void inTieudeDoanhThu(int j){
	gotoxy(30, j); cout<<"THANG";
	gotoxy(60, j); cout<<"DOANH THU";
}

void inHoaDonLietKe( int mangDt[]){
	for(int i = 0; i<12; i++){
		gotoxy(30, 3+i); 
		cout<< convertIntToString(i+1);
		gotoxy(60, 3+i); 
		cout<<mangDt[i] << endl;
	}
}

int TinhGiaTriHd(LCTHD listCthd){
	int ketQua = 0;
	for (int i = 0; i < listCthd.n; i++){
		ketQua = ketQua + listCthd.nodes[i].Dongia*listCthd.nodes[i].Soluong + (listCthd.nodes[i].Dongia*listCthd.nodes[i].Soluong*listCthd.nodes[i].VAT)/100;
	}
	return ketQua;
}

void TaoMangDoanhThu(NodeNV listNv, int namNhap, int mangDtThang[] ){
	if (listNv != NULL)
	{
		NODEHD *nodeHd = new NODEHD();
		nodeHd = listNv->nhanvien.listHD.first;
		
		while (nodeHd != NULL)
		{
			if ((nodeHd->infohd.ngaylap.nam == namNhap) && strcmp(nodeHd->infohd.loai,"X") == 0){
				int thangHd = nodeHd->infohd.ngaylap.thang;
				int giaTriHd = TinhGiaTriHd(nodeHd->infohd.cthd);
				mangDtThang[thangHd -1] =  mangDtThang[thangHd -1] + giaTriHd;
			}
			nodeHd = nodeHd->next;
		}
		
		TaoMangDoanhThu(listNv->pLeft, namNhap, mangDtThang);
		TaoMangDoanhThu(listNv->pRight, namNhap, mangDtThang);

	}
}

void InDoanhThu(NodeNV listNv){
	int namNhap;
	int mangDoanhThu[12] = {0,0,0,0,0,0,0,0,0,0,0,0};
	bool timThay = false;
	do{
		cout << "Nhap vao nam muon tinh doanh thu (0 la ket thuc): ";
		cin >> namNhap;
		if(namNhap != 0)
		{
			if(namNhap < 1000 || namNhap >9999){
				cout << "Nam nhap khong hop le!" <<endl;
			}else{
				TaoMangDoanhThu(listNv,namNhap,mangDoanhThu);
				timThay = true;
				break;
			}
		}
	}while(namNhap != 0);
	
	if(timThay){
		system("cls");                             
		cout <<"                             BANG THONG KE DOANH THU NAM "<<namNhap<<"                             " << endl;
		inTieudeDoanhThu(2);
		inHoaDonLietKe(mangDoanhThu);
	}
	
}

int SearchVatTuTrongCTHD(LCTHD lcthd, char *x){
	for (int i =0; i < lcthd.n ; i++)
     if (strcmp(lcthd.nodes[i].MAVT, x) == 0) return i;
  return -1;
}

void inDSVTtrongHD(LCTHD lcthd){
	
	 for (int i =0; i < dsVatTu.n ; i++){
	 	for (int j = 0; j < lcthd.n; j++){
	 		if (strcmp(dsVatTu.nodes[i]->MAVT, lcthd.nodes[j].MAVT) == 0){
	 			cout << "Ma VT: " << dsVatTu.nodes[i]->MAVT << endl;
     		cout << "Ten VT: "<< dsVatTu.nodes[i]->TENVT << endl;
     		cout << "Don vi tinh: "<< dsVatTu.nodes[i]->DVT << endl;
	 	}	
	 }
	}
}

void xoaVatTuTraHang(LCTHD &lcthd){
	char *x;
	char *trangthaitrahang = "0"; 
	do {
		cout << "Nhap vao Ma vat tu muon tra hang: ";
		cin >> x;
		if (strcmp(x,"0")==0) break;
		
		int i = SearchVatTuTrongCTHD(lcthd, x) ;
    	if (i==-1){
    		BaoLoi("Ma so vat tu khong co trong Chi tiet Hoa don");
    		break;
		}
    	else  
    	{
	     	strcpy(lcthd.nodes[i].status,trangthaitrahang);
	     	BaoLoi("Da da tra hang thanh cong.");
		}
		
	}while(strcmp(x,"0") != 0);
	
}

bool kiemTraNamNhuan(int year){
	if (((year % 4 == 0) && (year % 100!= 0)) || (year%400 == 0))
      return true;
   else
      return false;
}

int tinhKhoangCachThoiGian(date dateFrom, date dateTo){
	int dateOfYear=0, dateOfMonth=0, date=0;
	// Tinh khoang cach giua 2 nam
	for(int i =dateFrom.nam; i < dateTo.nam; i++){
		if(kiemTraNamNhuan(i)) dateOfYear +=366;
		else dateOfYear +=365;
	}
	
	//Tinh khoang cach giua 2 thang
	int a[]={31,28,31,30,31,30,31,31,30,31,30,31};
	if (kiemTraNamNhuan(dateFrom.nam)) a[1] =29;
	if (kiemTraNamNhuan(dateTo.nam)) a[1] =29;
	
	if(dateFrom.thang > dateTo.thang){
		for (int i =dateTo.thang; i < dateFrom.thang; i++){
			dateOfMonth -= a[i-1];
		}
	} else if(dateFrom.thang < dateTo.thang){
		for (int i= dateFrom.thang; i < dateTo.thang; i++){
			dateOfMonth += a[i-1];
		}
	}
	
	//Tinh khoang cach giua 2 ngay
	date = dateTo.ngay - dateFrom.ngay;
	
	return dateOfYear + dateOfMonth + date;
	
}

void traHang(NodeNV &p){
	string SoHd;
	HOADON hd;
	bool check;
	Date ngayTraHang = LayGioHeThong();
	int soNgay;
	NODEHD *nodeHd = new NODEHD();
	nodeHd = p->nhanvien.listHD.first;
		do{
			cout<< "Nhap so hoa don muon xem (0 la thoat ra) : ";
			fflush(stdin);
			getline(cin,SoHd);
	
			if(strcmp((char*)SoHd.c_str(),"0") != 0){
				
				check = DuyetCayNvTimHd(p,(char*)SoHd.c_str(),hd);
				int soNgay = tinhKhoangCachThoiGian(hd.ngaylap, ngayTraHang);
					if(check == true && soNgay < 3){ // neu tim duoc hoa don
						//in ra ds Vattu
						cout << "DANH SACH VAT TU TRONG HOA DON" << endl;
						inDSVTtrongHD(nodeHd->infohd.cthd);
						xoaVatTuTraHang(nodeHd->infohd.cthd);
						
					}else{
						cout << "Khong tim thay hoa don..."<<endl;
					}
				}
		}while(strcmp((char*)SoHd.c_str(),"0") != 0);
}

void NV(NodeNV &listNv){
	if (listNv != NULL){
		cout << listNv->nhanvien.MANV << endl;
		NV(listNv->pLeft);
		NV(listNv->pLeft);
	}
}
/** Menu Hoa Don **/
int MenuDongHoaDon()
{
	system("cls");
	int chon;
	while (1)
	{
		chon = MenuDong(thucdon_hd, so_item_hd);
		switch (chon)
		{
		case 1:
			gotoxy(10, 20);
			system("cls");
			LapHD_Nhap(pNVList,dsVatTu);
			break;
		case 2:
			gotoxy(10, 20);
			system("cls");
			LapHD_Xuat(pNVList,dsVatTu);
			break;
		case 3:
			gotoxy(10, 20);
			system("cls");
			XemHdTheoSoHd(pNVList);
			break;
		case 4:
			gotoxy(10, 20);
			system("cls");
			LietKeHoaDon(pNVList);
			break;
		case 5:
			gotoxy(10, 20);
			system("cls");
			InDoanhThu(pNVList);
			break;
		case 6:
			gotoxy(10, 20);
			system("cls");
			if (pNVList != NULL){
				traHang(pNVList);
			} else cout << "Chua co nhan vien, hoa don!";
			
			break;
		case 7:
			gotoxy(10, 20);
			system("cls");
			DocFileDsHoaDon(pNVList);
			break;
		case 8:
			gotoxy(10, 20);
			system("cls");
			LuuFileDSHD(pNVList);
			break;
		case so_item_hd:
			system("cls");
			return 0;
		}
		system("pause");
	}
}
