#include "mylib.h"

//----- KHAI BAO VAT TU -----

const int MAXLIST = 100;
struct VATTU
{
	char MAVT[10];
	char TENVT[10];
	char DVT[10];
	float SLT;
};

typedef struct DSVT
{
	int n;
	VATTU *nodes[MAXLIST];
};

//----- KHAI BAO CTHD -----

#define MAXLIST 20
struct CT_HOADON
{
	char MAVT[10];
	int Soluong;
	long Dongia;
	float VAT;
	char status[2];
};

typedef struct LCTHD
{
	int n = 0;
	CT_HOADON nodes[MAXLIST];
};

//----- KHAI BAO HOA DON -----

struct HoaDon
{
	char SoHD[20];
	Date ngaylap;
	char loai[2];
	LCTHD cthd;
};
typedef struct HoaDon HOADON;

struct nodehd
{
	HOADON infohd;
	struct nodehd *next;
};
typedef struct nodehd NODEHD;

struct listhd
{
	NODEHD *first;
	NODEHD *last;
};
typedef struct listhd ListHD;

//----- KHAI BAO NHAN VIEN -----

const int MAXNV = 100;
struct NHANVIEN
{
	int MANV;
	string HO;
	string TEN;
	string PHAI;
	ListHD listHD;
};

struct NODENV
{
	NHANVIEN nhanvien;
	struct NODENV *pLeft;
	struct NODENV *pRight;
};
typedef struct NODENV *NodeNV;

NodeNV pNVList;
