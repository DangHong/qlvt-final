#pragma once
#ifndef _MYLIB_H_
#define _MYLIB_H_

#include <iostream>
#include <conio.h>
#include <string>
#include <Windows.h>
#include <ctime>
#include <fstream>
#include <sstream>
//#include<cstdlib>
#include <unistd.h>
#include <math.h>
using namespace std;

#define Enter 13
#define PASSWORD "abcdef"

const int so_item_title = 6;
const int so_item = 4;
const int so_item_nv = 7;
const int so_item_hd = 9;
const int so_item_lop = 8;
const int so_item_vt = 8;

const int dong = 10;
const int cot = 40;
const int Up = 72;
const int Down = 80;

string DANHSACHHOADON[3] = {"SO HOA DON", "NGAY LAP", "LOAI"};
string CTHOADON[4] = {"MA VT", "SO LUONG", "DON GIA", "VAT (%)"};

struct date
{
	int ngay;
	int thang;
	int nam;
};
typedef struct date Date;

int indexSoHd = 0;
int mangSoHdNgauNhien[100000];

int indexSoVt = 0;
int mangSoVtNgauNhien[100000];


char Title[so_item_title][50] = {
	"CTDL&GT",
	"GVHD: LUU NGUYEN KY THU",
	"TEN DE TAI: QUAN LI VAT TU",
	"SVTH: DANG KIM HONG - TRAN THI NGOC MY",
	"MSSV: N14DCPT102    - N14DCPT029",
	"LOP: D14CQPU01-N"};

char thucdon[so_item][50] = {
	"1. QUAN LI VAT TU			",
	"2. QUAN LI NHAN VIEN 			",
	"3. QUAN LI HOA DON				",
	"0. THOAT						"};

char thucdon_nv[so_item_nv][50] = {
	"1. Them Nhan Vien					",
	"2. Xem Danh Sach Nhan vien (ASC)	",
	"3. Chinh Sua Nhan Vien				",
	"4. Xoa Nhan Vien					",
	"5. Open File						",
	"6. Save File						",
	"0. Thoat							",
};

char thucdon_hd[so_item_hd][50] = {
	"1. LAP HOA DON NHAP								",
	"2. LAP HOA DON XUAT								",
	"3. XEM HOA DON THEO SO HOA DON						",
	"4. XEM HOA DON THEO NHAN VIEN VIEN VA THOI GIAN	",
	"5. IN DOANH THU THEO THANG CUA TUNG NAM			",
	"6. TRA HANG										",
	"7. OPEN FILE										",
	"8. SAVE FILE										",
	"0. THOAT											"};

char thucdon_vt[so_item_vt][50] = {
	"1. Nhap Vat Tu					",
	"2. In Danh Sach Vat Tu			",
	"3. Them Vat Tu					",
	"4. Chinh Sua Vat Tu			",
	"5. Xoa Vat Tu					",
	"6. Open File					",
	"7. Save File					",
	"0. Thoat						"};

void gotoxy(short x, short y)
{
	HANDLE hConsoleOutput;
	COORD Cursor_an_Pos = {x, y};
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hConsoleOutput, Cursor_an_Pos);
}

int wherex(void)
{
	HANDLE hConsoleOutput;
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO screen_buffer_info;
	GetConsoleScreenBufferInfo(hConsoleOutput, &screen_buffer_info);
	return screen_buffer_info.dwCursorPosition.X;
}

int wherey(void)
{
	HANDLE hConsoleOutput;
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO screen_buffer_info;
	GetConsoleScreenBufferInfo(hConsoleOutput, &screen_buffer_info);
	return screen_buffer_info.dwCursorPosition.Y;
}
void clreol()
{
	COORD coord;
	DWORD written;
	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &info);
	coord.X = info.dwCursorPosition.X;
	coord.Y = info.dwCursorPosition.Y;
	FillConsoleOutputCharacter(GetStdHandle(STD_OUTPUT_HANDLE), ' ',
							   info.dwSize.X - info.dwCursorPosition.X * info.dwCursorPosition.Y, coord, &written);
	gotoxy(info.dwCursorPosition.X + 1, info.dwCursorPosition.Y + 1);
}

void SetColor(WORD color)
{
	HANDLE hConsoleOutput;
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO screen_buffer_info;
	GetConsoleScreenBufferInfo(hConsoleOutput, &screen_buffer_info);

	WORD wAttributes = screen_buffer_info.wAttributes;
	color &= 0x000f;
	wAttributes &= 0xfff0;
	wAttributes |= color;

	SetConsoleTextAttribute(hConsoleOutput, wAttributes);
}
void SetBGColor(WORD color)
{
	HANDLE hConsoleOutput;
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_SCREEN_BUFFER_INFO screen_buffer_info;
	GetConsoleScreenBufferInfo(hConsoleOutput, &screen_buffer_info);

	WORD wAttributes = screen_buffer_info.wAttributes;
	color &= 0x000f;
	color <<= 4;
	wAttributes &= 0xff0f;
	wAttributes |= color;

	SetConsoleTextAttribute(hConsoleOutput, wAttributes);
}
void setcolor(int background, int textcolor)
{
	SetBGColor(background);
	SetColor(textcolor);
}

void hidecursor()
{
	HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO info;
	info.dwSize = 100;
	info.bVisible = FALSE;
	SetConsoleCursorInfo(consoleHandle, &info);
}

void khung(int x, int y, int width, int height)
{
	gotoxy(x, y);
	printf("%c", 218);
	for (int i = 1; i < width; i++)
	{
		gotoxy(x + i, y);
		printf("%c", 196);
	}
	gotoxy(x, y + height);
	printf("%c", 192);
	for (int j = 1; j < height; j++)
	{
		gotoxy(x, y + j);
		printf("%c", 179);
	}
	gotoxy(x + width, y);
	printf("%c", 191);
	for (int i = 1; i < height; i++)
	{
		gotoxy(x + width, y + i);
		printf("%c", 179);
	}
	gotoxy(x + width, y + height);
	printf("%c", 217);
	for (int j = 1; j < width; j++)
	{
		gotoxy(x + j, y + height);
		printf("%c", 196);
	}
}

void thongtin()
{
	gotoxy(50, 1);
	for (int i = 0; i < so_item_title; i++)
	{
		cout << Title[i];
		gotoxy(40, 4 + i);
	}
}
void Normal()
{
	SetColor(3);
	SetBGColor(0);
}
void HighLight()
{
	SetColor(0);
	SetBGColor(15);
}
int MenuDong(char td[][50], int num)
{
	Normal();
	system("cls");
	thongtin();
	int chon = 0;
	int i;
	for (i = 0; i < num; i++)
	{
		gotoxy(cot, dong + i);
		cout << td[i];
	}

	HighLight();
	gotoxy(cot, dong + chon);
	cout << td[chon];
	char kytu;
	do
	{
		kytu = _getch();
		if (kytu == 0)
			kytu = _getch();
		switch (kytu)
		{
		case Up:
			if (chon + 1 > 1)
			{
				Normal();
				gotoxy(cot, dong + chon);
				cout << td[chon];
				chon--;
				HighLight();
				gotoxy(cot, dong + chon);
				cout << td[chon];
			}
			break;
		case Down:
			if (chon + 1 < num)
			{
				Normal();
				gotoxy(cot, dong + chon);
				cout << td[chon];
				chon++;
				HighLight();
				gotoxy(cot, dong + chon);
				cout << td[chon];
			}
			break;
		case 13:
			return chon + 1;
		} // end switch
	} while (1);
}

void BaoLoi(char *s)
{
	int x = wherex(), y = wherey();
	gotoxy(10, 24);
	cout << s;
	Sleep(4000);
	gotoxy(10, 24);
//	clreol();
	gotoxy(x, y);
}
void NhapChuoi(char *tieude, char *S)
{
	cout << tieude;
	fflush(stdin);
	do
		gets(S);
	while (strcmp(S, "") == 0);
}

void NhapTonKho(char *tieude, float &num)
{
	cout << tieude;
	fflush(stdin);
	cin >> num;
}

//-------------------------MY ADD-------------------------

void VeVien(int x, int y, int dai, int rong, int mau, string s)
{
	for (int i = 1; i <= dai; i++)
	{
		gotoxy(x + i, y);
		cout << char(196);
	}

	for (int i = 1; i <= rong; i++)
	{
		gotoxy(x + 1, y + i);
		for (int j = 1; j <= dai; j++)
		{
			SetBGColor(mau);
			cout << " ";
		}
	}
	gotoxy(x + (dai - s.length()) / 2, rong / 2 + y);
	cout << s;
	SetBGColor(0);
	for (int i = 1; i <= dai; i++)
	{
		gotoxy(x + i, rong + y + 1);
		cout << char(196);
	}
}

void VeBangDanhSachChay(int x, int y, int socot, int sodong, int kichthuoccot)
{
	/*SetColor(WHITE);
	SetBGColor(BLACK);*/
	// Tao bang
	// 218 --- 196 --- 194 --- 196 --- 191
	// 179             179             179
	// 195 --- 196 --- 197 --- 196 --- 180
	// 179             179             179
	// 192 --- 196 --- 193 --- 196 --- 217
	for (int i = 0; i < sodong * 2 + 1; i++)
	{
		gotoxy(x, y + i);
		for (int j = 0; j < socot * 2 + 1; j++)
		{
			// Dong dau
			if (i == 0)
			{
				if (j == 0)
					cout << char(218); // dau dong
				else if (j == socot * 2)
					cout << char(191); // cuoi dong
				else if (j % 2 == 0)
					cout << char(194); // phan chia cac cot
				else
					for (int k = 0; k < kichthuoccot; k++)
						cout << char(196);
			}
			// Dong cuoi
			else if (i == sodong * 2)
			{
				if (j == 0)
					cout << char(192);
				else if (j == socot * 2)
					cout << char(217);
				else if (j % 2 == 0)
					cout << char(193);
				else
					for (int k = 0; k < kichthuoccot; k++)
						cout << char(196);
			}
			// Dong Giua
			else
			{
				if (i == 2)
				{
					if (j == 0)
						cout << char(195);
					else if (j == socot * 2)
						cout << char(180);
					else if (j == 0 || j == socot * 2)
						cout << char(179);
					else if (j % 2 == 0)
						cout << char(197);
					else
						for (int k = 0; k < kichthuoccot; k++)
							cout << char(196);
				}
				else if (j % 2 == 0)
					cout << char(179);
				else
					for (int k = 0; k < kichthuoccot; k++)
						cout << " ";
			}
		}
	}
}

void TaoFormNhap(string s, int x, int y, int kichthuoccot)
{
	VeBangDanhSachChay(x, y, 1, 5, kichthuoccot);
	gotoxy(x + 1 + (kichthuoccot - s.length()) / 2, y + 1);
	cout << s;
}

void TaoThongBaoVaMat(int x, int y, string s)
{
	gotoxy(x, y);
	cout << s;
	sleep(1);
	gotoxy(x, y);
	cout << "                                                                          ";
}

void ThongBao(int x, int y, string s)
{
	gotoxy(x, y);
	cout << s;
}

void TaoMangSoNgauNhien(int increaseArray[])
{
	// Tao mang tang dan 1-100000
	for (int k = 0; k < 100000; k++)
	{
		increaseArray[k] = k + 1;
	}

	//Xao tron 1 cach ngau nhien
	for (int i = 99999; i > 0; --i)
	{
		// Lay vi tri random
		int j = rand() % i;
		// Hoan doi i voi j
		int temp = increaseArray[i];
		increaseArray[i] = increaseArray[j];
		increaseArray[j] = temp;
	}
}

string convertIntToString(int num)
{
	stringstream ss;
	ss << num;
	return ss.str();
}

char *convertIntToChar(int num)
{

	stringstream ss;
	ss << num;
	//ep kieu string thanh char
	return (char *)ss.str().c_str();
}

string convertLongToString(long num)
{
	stringstream ss;
	ss << num;
	return ss.str();
}

string convertFloatToString(float num)
{

	stringstream ss;
	ss << num;
	return ss.str();
}

float convertStringToFloat(string str)
{

	stringstream ss(str);
	float num;
	ss >> num;
	return num;
}

int convertStringToInt(string str)
{

	stringstream ss(str);
	int num;
	ss >> num;
	return num;
}

string ChuyenDateSangChuoi(Date date)
{
	string str = convertIntToString(date.ngay) + '/' + convertIntToString(date.thang) + '/' + convertIntToString(date.nam);
	return str;
}

Date ChuyenChuoiSangDate(char *str)
{
	Date date = {};
	char *p;
	p = strtok(str, "/");
	date.ngay = convertStringToInt(p);
	p = strtok(NULL, "/");
	date.thang = convertStringToInt(p);
	p = strtok(NULL, "/");
	date.nam = convertStringToInt(p);

	return date;
}

int DemKyTu(string s)
{
	int dem=0;
	for(int i=0;i<s.length();i++)
	{
		if(s[i]!=' ')
		{
			dem++;
		}
	}
	return dem;
}

bool CheckInPutLaSo(string s)
{
	for(int i =0; i< s.length();i++)
	{
		if(s[i] >='0' && s[i] <= '9' ) return true;
	}
	return false;
}

//
bool CheckKieuDate(char* cDate, Date &date){
	char* p;
	p = strtok(cDate, "/");
	if(p == NULL || CheckInPutLaSo(p) == false || DemKyTu(p) != 2 ){
		return false;
	}else{
		date.ngay = convertStringToInt(p);
	}
	
	p = strtok(NULL, "/");
	if(p == NULL || CheckInPutLaSo(p) == false || DemKyTu(p) != 2 ){
		return false;
	}else{
		date.thang = convertStringToInt(p);
	}
	
	p = strtok(NULL, "/");
	if(p == NULL || CheckInPutLaSo(p) == false || DemKyTu(p) != 4 ){
		return false;
	}else{
		date.nam = convertStringToInt(p);
	}
	
	if (date.thang < 1 || date.thang > 12 ){
		return false;
	}else if (date.thang == 2){
		if (date.ngay < 1 || date.ngay > 29)
			return false;
	}else if (date.thang == 1 || date.thang == 3 || date.thang == 5 || date.thang == 7 || date.thang == 8 || date.thang == 10 || date.thang == 12){
			if (date.ngay < 1 || date.ngay > 31)
				return false;
	}else{
		if (date.ngay < 1 || date.ngay > 30)
			return false;
	}
	
	return true;
}

bool NhapNgayThang(char* tieuDe, Date &date){
	string strDate;
	do{
		cout << tieuDe;
		fflush(stdin);
		getline(cin,strDate);
		if(strDate != "0")
		{
			if (CheckKieuDate((char*)strDate.c_str(), date) == true){
				return true;
			}else{
				cout << "Dinh dang khong dung"<<endl;
			}
		}
	}while(strDate != "0");
	
	return false;
}

#endif //
