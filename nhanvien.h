#include "cthd.h"

using namespace std;

void Initialize(NodeNV &p)
{
	p = NULL;
}

NodeNV TimNV(NodeNV pNV, int x)
{
	NodeNV p;
	p = pNV;
	while (p != NULL && p->nhanvien.MANV != x)
		if (x < p->nhanvien.MANV)
			p = p->pLeft;
		else
			p = p->pRight;
	return (p);
}

void ThemNV(NodeNV &p, NHANVIEN nv)
{
	if (p == NULL)
	{
		p = new NODENV;
		p->nhanvien = nv;
		p->pLeft = NULL;
		p->pRight = NULL;
	}
	else
	{
		if (p->nhanvien.MANV < nv.MANV)
			ThemNV(p->pRight, nv);
		else if (p->nhanvien.MANV > nv.MANV)
			ThemNV(p->pLeft, nv);
	}
}

void NhapNV(NodeNV &p)
{
	NHANVIEN nv;
	do
	{
		cout << "Nhap Ma Nhan vien: ";
		cin >> nv.MANV;
		if (nv.MANV != 0)
		{
			if (TimNV(p, nv.MANV) == NULL)
			{
				cout << "Nhap Ho Nhan vien: ";
				cin >> nv.HO;
				cout << "Nhap Ten Nhan vien: ";
				cin >> nv.TEN;
				cout << "Nhap Gioi tinh: ";
				cin >> nv.PHAI;
				ThemNV(p, nv);
			}
			else
			{
				BaoLoi("Ma nhan vien bi trung");
				continue;
			}
		}
	} while (nv.MANV != 0);
}
// Khai bao struct DSNV mang con tro
struct DSNV
{
	NHANVIEN *DSNhanvien;
	int n;
};
int InsertOrderNV(DSNV &ds, NHANVIEN nv)
{

	int j, k;
	string TenHoNV = nv.TEN + nv.HO;
	string TenHoDSNV;
	if (ds.n == MAXNV)
		return 0;
	for (j = 0; j < ds.n; j++)
	{
		TenHoDSNV = ds.DSNhanvien[j].TEN + ds.DSNhanvien[j].HO;
		if (TenHoDSNV.compare(TenHoNV) > 0)
		{
			break;
		}
	}
	for (k = ds.n - 1; k >= j; k--)
		ds.DSNhanvien[k + 1] = ds.DSNhanvien[k];
	ds.DSNhanvien[j] = nv;
	ds.n++;
	return 1;
}

void inTieudeNV(int j)
{
	gotoxy(2, j - 1);
	cout << "STT";
	gotoxy(10, j - 1);
	cout << "MA NHAN VIEN";
	gotoxy(30, j - 1);
	cout << "HO NHAN VIEN";
	gotoxy(50, j - 1);
	cout << "TEN NHAN VIEN";
	gotoxy(70, j - 1);
	cout << "PHAI";
}
void DuyetCayNV(NodeNV p)
{
	DSNV dsnv;
	int j = 3;
	dsnv.DSNhanvien = new NHANVIEN[MAXNV];
	dsnv.n = 0;
	const int STACKSIZE = 500;
	NodeNV Stack[STACKSIZE];
	NodeNV root = p;
	int sp = -1; // Khoi tao Stack rong
	do
	{
		while (root != NULL)
		{
			Stack[++sp] = root;
			root = root->pLeft;
		}
		if (sp != -1)
		{
			root = Stack[sp--];
			InsertOrderNV(dsnv, root->nhanvien);
			root = root->pRight;
		}
		else
			break;

	} while (1);

	gotoxy(30, 0);
	cout << "DANH SACH NHAN VIEN" << endl;
	inTieudeNV(j);
	if (dsnv.n > 0)
	{
		for (int i = 0; i < dsnv.n; i++)
		{
			gotoxy(2, j);
			cout << i + 1;
			gotoxy(10, j);
			cout << dsnv.DSNhanvien[i].MANV;
			gotoxy(30, j);
			cout << dsnv.DSNhanvien[i].HO;
			gotoxy(50, j);
			cout << dsnv.DSNhanvien[i].TEN;
			gotoxy(70, j);
			cout << dsnv.DSNhanvien[i].PHAI;
			cout << endl;
			j++;
		}
	}
	else
	{
		BaoLoi("Danh sach chua co nhan vien!");
	}
}

void SaveFileNV(NodeNV root, char *filename)
{
	NodeNV Stack[MAXNV];
	NodeNV p = root;
	int sp = -1; // Khoi tao Stack rong
	ofstream ofs;
	ofs.open(filename, ios_base::out);
	do
	{
		while (p != NULL)
		{
			Stack[++sp] = p;
			p = p->pLeft;
		}
		if (sp != -1)
		{
			p = Stack[sp--];
			ofs << p->nhanvien.MANV << endl;
			ofs << p->nhanvien.HO << endl;
			ofs << p->nhanvien.TEN << endl;
			ofs << p->nhanvien.PHAI << endl;
			p = p->pRight;
		}
		else
			break;
		BaoLoi("Da luu file thanh cong!");
	} while (1);
	ofs.close();
}

void OpenFileNV(NodeNV &root, char *filename)
{
	int maxnode, i;
	int ms;
	string buffer;
	NHANVIEN nv;

	ifstream ifs;
	ifs.open(filename, ios_base::in);
	while (!ifs.eof())
	{
		getline(ifs, buffer);
		if (buffer != "")
		{
			// atoi de parse tu kieu char ra kieu int
			nv.MANV = atoi(buffer.c_str());

			ifs >> buffer;
			nv.HO = buffer;
			ifs >> buffer;
			nv.TEN = buffer;
			ifs >> buffer;
			nv.PHAI = buffer;
			ThemNV(root, nv);
			BaoLoi("Da load xong du lieu tu file!");
		}
	}
	ifs.close();
}

void remove_case_3(NodeNV &r)
{
	NodeNV rp;
	if (r->pLeft != NULL)
		remove_case_3(r->pLeft);
	//den day r la nut cuc trai cua cay con ben phai co nut goc la rp}
	else
	{
		rp->nhanvien.MANV = r->nhanvien.MANV; //Chep noi dung cua r sang rp ";
		rp->nhanvien.TEN = r->nhanvien.TEN;   //  de lat nua free(rp)
		rp = r;
		r = rp->pRight;
	}
}
void remove(int x, NodeNV &p)
{
	if (p == NULL)
		printf("Khong tim thay");
	else if (x < p->nhanvien.MANV)
		remove(x, p->pLeft);
	else if (x > p->nhanvien.MANV)
		remove(x, p->pRight);
	else // p->key = x
	{
		NodeNV rp = p;
		if (rp->pRight == NULL)
			p = rp->pLeft;
		// p l� n�t l� hoac la nut chi co cay con ben trai
		else if (rp->pLeft == NULL)
			p = rp->pRight; // p l� nut co cay con ben phai
		else
			remove_case_3(rp->pRight);
		delete rp;
	}
}

void layMangInfo(NodeNV &p, string info, int mangInfo[], int &i)
{
	if (p != NULL)
	{
		if (p->nhanvien.TEN == info)
		{
			mangInfo[i] = p->nhanvien.MANV;
			i++;
		}
		layMangInfo(p->pLeft, info, mangInfo, i);
		layMangInfo(p->pRight, info, mangInfo, i);
	}
}

void xoaNhanVientheoInfo(NodeNV &p, string info)
{
	int i = 0;
	int mangInfo[100];

	layMangInfo(p, info, mangInfo, i);
	for (int j = 0; j < i; j++)
	{
		remove(mangInfo[j], p);
	}
}

void SuaNhanVien(NodeNV &p, NHANVIEN nv)
{
	char c[2];
	int x;
	cout << "Nhap vao ma nhan vien muon chinh sua: ";
	cin >> x;
	NodeNV pNV;
	pNV = TimNV(p, x);
	if (x == 0)
		return;
	if (pNV != NULL)
	{
		cout << "Ma NV: " << pNV->nhanvien.MANV << endl;
		cout << "Ho NV: " << pNV->nhanvien.HO << endl;
		cout << "Ten NV: " << pNV->nhanvien.TEN << endl;
		cout << "Gioi tinh: " << pNV->nhanvien.PHAI << endl;
		cout << endl;
	}

	cout << "Nhap Ho Nhan vien: ";
	cin >> nv.HO;
	cout << "Nhap Ten Nhan vien: ";
	cin >> nv.TEN;
	cout << "Nhap Gioi tinh Nhan vien: ";
	cin >> nv.PHAI;
	nv.MANV = pNV->nhanvien.MANV;
	NhapChuoi("Ban co chan chac muon luu?(c/k)", c);
	if (strcmp(c, "c") == 0)
	{
		xoaNhanVientheoInfo(p, pNV->nhanvien.TEN);

		ThemNV(pNVList, nv);
	}
}

/** Menu Nhan Vien **/
int MenuDongNhanVien()
{
	//pNVList = NULL;
	char c[2];
	NHANVIEN nv;
	//Initialize(pNVList);
	BOOL flag = false;
	char filename[80] = "DSNV.TXT";
	system("cls");
	int chon;
	while (1)
	{
		chon = MenuDong(thucdon_nv, so_item_nv);
		switch (chon)
		{
		case 1:
			gotoxy(10, 20);
			system("cls");
			NhapNV(pNVList);
			break;
		case 2:
			gotoxy(10, 20);
			system("cls");
			DuyetCayNV(pNVList);
			break;
		case 3:
			gotoxy(10, 20);
			system("cls");
			SuaNhanVien(pNVList, nv);
			break;
		case 4:
			gotoxy(10, 20);
			system("cls");

			do
			{
				cout << "Nhap vao Ten Nhan Vien can xoa: ";
				cin >> nv.TEN;
				if (pNVList->nhanvien.TEN == nv.TEN)
				{
					cout << "MA NV: " << pNVList->nhanvien.MANV << endl;
					cout << "HO	NV: " << pNVList->nhanvien.HO << endl;
					cout << "TEN NV: " << pNVList->nhanvien.TEN << endl;
					cout << "GIOI TINH: " << pNVList->nhanvien.PHAI << endl;
					cout << "Ban co muon xoa khong?(c/k)";
					cin >> c;
					if (strcmp(c, "c") == 0)
					{
						xoaNhanVientheoInfo(pNVList, nv.TEN);
						break;
					}
				}
				else
				{
					BaoLoi("Khong tim thay trong danh sach.");
					flag = true;
				}
			} while (flag);
			break;
		case 5:
			gotoxy(10, 20);
			system("cls");
			OpenFileNV(pNVList, filename);
			break;
		case 6:
			gotoxy(10, 20);
			system("cls");
			SaveFileNV(pNVList, filename);
			break;
		case so_item_nv:
			return 0;
		}
		system("pause");
	}
}
