#include "khaibao.h"

using namespace std;

DSVT dsVatTu;
void inTieudeVatTu(int j){
	gotoxy(2,j-1); cout<< "STT";
	gotoxy(10, j-1); cout<<"MA VAT TU";
	gotoxy(30, j-1); cout<<"TEN VAT TU";
	gotoxy(50, j-1); cout<<"DON VI";
	gotoxy(60, j-1); cout<<"SO LUONG TON";
}

int SearchVatTu(DSVT &ds, char *x) {
  for (int i =0; i < ds.n ; i++)
     if (strcmp(ds.nodes[i]->MAVT, x) == 0) return i;
  return -1;
}

int SearchVatTutheoInfo(DSVT &ds, char *info) {
  for (int i = 0; i < ds.n ; i++)
     if (strcmp(ds.nodes[i]->TENVT, info) == 0) return i;
  return -1;
}

int InsertOrderVatTu(DSVT &ds, VATTU vt){
	int j, k;
	if (ds.n == MAXLIST ) return 0;
	while (ds.n < MAXLIST)
 {  
   cout << "Nhap ma so (0 la ket thuc) : " ; 
   string x;
   fflush(stdin);
   getline(cin,x);
   strcpy(vt.MAVT,x.c_str());
   if (strcmp(vt.MAVT, "0") == 0) break;
   if (SearchVatTu(ds, vt.MAVT)>=0){
       BaoLoi ("Ma vat tu bi trung");
       continue;
   }
    NhapChuoi (  "Nhap Ten Vat tu: ", vt.TENVT); 
    NhapChuoi (  "Nhap Don vi tinh: ", vt.DVT);
	NhapTonKho ( "Nhap So luong ton: ", vt.SLT);

    for (j=0; j < ds.n && ds.nodes[j]->TENVT[0] < vt.TENVT[0] ; j++);
	for (k = ds.n-1; k >=j  ; k--)
		ds.nodes[k+1]= ds.nodes[k];
	ds.nodes[j] = new VATTU;
	*ds.nodes[j]= vt; 
	ds.n++;	
}
	return 1;
	
}

void NhapDSVT(DSVT &ds, int flag){
	VATTU *p;
	VATTU vattu;
	p = &vattu;

	if(flag == 0){
		while(ds.n > 0){
			delete ds.nodes[(ds.n)-1];
			ds.n--;
		}
	}
	
	while (ds.n <MAXLIST)
 {  
   cout << "Nhap ma so (0 la ket thuc) : " ; 
   string x;
   fflush(stdin);
   getline(cin,x);
   strcpy(vattu.MAVT,x.c_str());
//   cout <<"VT" << TaoMaSoVatTu(vattu) << endl;
   if (strcmp(vattu.MAVT, "0") == 0) break;
   if (SearchVatTu(ds, vattu.MAVT)>=0){
       BaoLoi ("Ma vat tu bi trung");
       continue;
   }
    NhapChuoi (  "Nhap Ten Vat tu: ", vattu.TENVT);
    NhapChuoi (  "Nhap Don vi tinh: ", vattu.DVT);
	NhapTonKho( "Nhap So luong ton: ", vattu.SLT);
	
    ds.nodes[ds.n]= new VATTU;
    *ds.nodes[ds.n] = *p;
    ds.n++;
}
}

void SapxepVT(DSVT &dsvt)
{
	VATTU *t;
	for(int j=1;j < dsvt.n;j++)
	{
		for(int z=dsvt.n-1; z >=j; z--)
		{
			if(dsvt.nodes[z-1]->TENVT[0] > dsvt.nodes[z]->TENVT[0])
			{
				t = dsvt.nodes[z-1];
				dsvt.nodes[z-1] = dsvt.nodes[z];
				dsvt.nodes[z]= t;
			}
		}
	}
}


void LietKeVatTu(DSVT &dsvt){
	int j = 3;
	gotoxy(30,0); cout <<"DANH SACH VAT TU" << endl;
	inTieudeVatTu(j);
	SapxepVT(dsvt);
	for (int i = 0; i< dsvt.n;i++){
		gotoxy(2, j); cout<< i+1;
		gotoxy(10, j); cout<<dsvt.nodes[i]->MAVT;
		gotoxy(30, j); cout<<dsvt.nodes[i]->TENVT;
		gotoxy(50, j); cout<<dsvt.nodes[i]->DVT;
		gotoxy(60, j); cout<<dsvt.nodes[i]->SLT;
		cout<<endl;
		j++;
	}
}

void XoaVatTu(DSVT &ds, char *maso){
    int i = SearchVatTu(ds,maso) ;
    if (i==-1) BaoLoi("Ma so vat tu khong co trong danh sach");
    else  
    {
	   		delete ds.nodes[i];
	     	for (int j=i+1; j <ds.n; j++)
	       	ds.nodes[j-1]=ds.nodes[j];
	     	ds.n--;  
	}
}

void SaveFile(DSVT ds,char *filename) {
 FILE * f;
 if ((f=fopen(filename,"wb"))==NULL)
 {  BaoLoi ("Loi mo file de ghi"); return ;
 }
 for (int i=0; i < ds.n; i++)
   fwrite (ds.nodes[i], sizeof (VATTU), 1, f);
 fclose(f);
 BaoLoi ("Da ghi xong danh sach vao file");

}

void OpenFile(DSVT &ds, char *filename) {
 FILE * f;
 VATTU vt;
 
 if ((f=fopen(filename,"rb"))==NULL)
 {  BaoLoi ("Loi mo file de doc"); return ;
 }
 ds.n =0;
 while  (fread (&vt, sizeof (VATTU), 1, f)!=0){
 	 ds.nodes[ds.n] = new VATTU;
    *ds.nodes[ds.n] = vt;
    ds.n++;
 }  
 fclose(f);
 BaoLoi ("Da load xong danh sach vao bo nho");
}

int SuaVatTu(DSVT &ds, VATTU vt){
	VATTU *p;
	p = &vt;
	int j = 2;
	char c[2];
	cout << "Nhap ma vat tu muon chinh sua: ";
	string x;
    fflush(stdin);
   getline(cin,x);
   strcpy(vt.MAVT,x.c_str());
   if (strcmp(vt.MAVT, "0") == 0) return 0;
   if (SearchVatTu(ds, vt.MAVT)>=0){	
		inTieudeVatTu(j);
		gotoxy(10, j); cout<< vt.MAVT;
		gotoxy(30, j); cout<<ds.nodes[SearchVatTu(ds, vt.MAVT)]->TENVT;
		gotoxy(50, j); cout<<ds.nodes[SearchVatTu(ds, vt.MAVT)]->DVT;
		gotoxy(60, j); cout<<ds.nodes[SearchVatTu(ds, vt.MAVT)]->SLT;
		cout<<endl;
   }
   
	NhapChuoi (  "Nhap Ten Vat tu: ", vt.TENVT);
	NhapChuoi (  "Nhap Don vi tinh: ", vt.DVT);
	cout<<"So luong ton: " << ds.nodes[SearchVatTu(ds, vt.MAVT)]->SLT << endl;
	cout << "Ban co chan chac muon luu?(c/k)";
   gets(c);
   if(strcmp(c, "c") == 0){
	   	vt.SLT = (float)ds.nodes[SearchVatTu(ds, vt.MAVT)]->SLT;
		XoaVatTu(ds, vt.MAVT);
		
	    ds.nodes[ds.n]= new VATTU;
	    *ds.nodes[ds.n] = *p;
	    ds.n++;
   } else return 0;
	 
}

/** Menu Vat Tu **/
int MenuDongVatTu(){
	char filename[80]="DSVT.txt";
	  char maso[10]; VATTU Vattu; char c[2];
  system("cls");
	int chon;
	while (1) {
		chon = MenuDong(thucdon_vt, so_item_vt);
		switch (chon) {
		case 1: gotoxy(10, 20);
			system("cls");
			NhapDSVT(dsVatTu, 0);
			break;
		case 2: gotoxy(10, 20);
			system("cls");
			LietKeVatTu(dsVatTu);
			break;
		case 3: gotoxy(10, 20);
			system("cls");
			InsertOrderVatTu(dsVatTu, Vattu);
			break;
		case 4: gotoxy(10, 20);
			system("cls");
			SuaVatTu(dsVatTu,Vattu);
			break;
		case 5: gotoxy(10,20);
			system("cls");
			gotoxy(2,2); LietKeVatTu(dsVatTu);
			gotoxy(80,2);
			cout << "Nhap ma so vat tu can xoa:";
			cin >> maso;
			gotoxy(80,4);
			cout << "Ban co muuon xoa khong?(c/k)";
			cin >> c;
			if (strcmp(c, "c") == 0) {
				XoaVatTu(dsVatTu,maso);
			}		
			break;
		case 6: gotoxy(10, 20);
			system("cls");
			OpenFile(dsVatTu, filename);
			break;
		case 7: gotoxy(10, 20);
			system("cls");
			SaveFile(dsVatTu, filename);
			break;
		case so_item_vt: return 0;
		}
		system("pause");
	}
}

